import Banner from "../components/Banner";
import Highlights from "../components/Highlights";


export default function Home(){
	const data = {
		title: "Valvrave Online Store",
		content: "Get the latest games here!",
		destination: "/products",
		label: "Shop Now"
	}

	return(
		<>
			<Banner data={data}/>
        	
			<Highlights />
        </>
	)
}
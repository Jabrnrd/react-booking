import {useEffect, useState, useContext} from "react";
import {Navigate} from "react-router-dom";
import UserContext from "../UserContext";

import ProductCard from "../components/ProductCard";

export default function Products(){


	 const [products, setProducts] = useState([]);


	const {user} = useContext(UserContext);


	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/products`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProducts(data.map(product =>{
				return(
					<ProductCard key={product._id} productProp={product} />
				)
			}))
		})
	}, [])


	return(
		(user.isAdmin)
		?
			<Navigate to="/admin" />
		:
		<>
			<h1 className="text-center mt-5 mb-5 text-light">Products</h1>
			{products}
		</>
	)
}
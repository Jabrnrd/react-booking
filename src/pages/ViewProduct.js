import { useState, useEffect, useContext } from "react";
import { Link, useParams, useNavigate } from "react-router-dom";

import Swal from "sweetalert2";
import { Container, Card, Button, Row, Col } from "react-bootstrap";

import UserContext from "../UserContext";

export default function ViewProduct(){

	//To check if their is already a logged in user. Change the button from "Enroll" to "Login" if the user is not logged in.
	const { user } = useContext(UserContext);

	//Allow us to gain access to methods that will redirect a user to different page after enrolling a course.
	const navigate = useNavigate();

	// "useParams" hook allows us to retrieve the courseId passed via URL.
	const { productId } = useParams();

	// Create state hooks to capture the information of a specific course and display it in our application.
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [stocks, setStocks] = useState(0);
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(1);


	const buy = (productId) =>{
		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					title: "Succesfully Purchased",
					icon: "success",
					text: "You have successfully purchased this game."
				})
				navigate("/products");

			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}
		})
	}

	useEffect(() =>{
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setStocks(data.stocks);
			setPrice(data.price);
		})

	}, [productId])

		return(
			<Container className="m-5 opacity: 0.6">
				<Row>
					<Col lg={{ span: 6, offset: 3 }}>
						<Card>
							<Card.Body className="text-center">
								<Card.Title>{name}</Card.Title>
								<Card.Subtitle>Description:</Card.Subtitle>
								<Card.Text>{description}</Card.Text>
								<Card.Subtitle>Stocks:</Card.Subtitle>
								<Card.Text>{stocks}</Card.Text>
								<Card.Subtitle>Price:</Card.Subtitle>
								<Card.Text>Php {price}</Card.Text>
								<div className="d-grid gap-2">
								{
									(stocks !== 0 )
									?
										(user.id !== null)
										?
										<>
				                            <Card.Subtitle>Total Amount:</Card.Subtitle>
				                            <Card.Text>Php {price}</Card.Text>
											<Button variant="danger" size="lg" onClick={() => buy(productId)}>Buy now</Button>
										
										</>
										:
											<Button as={Link} to="/login" variant="danger" size="lg">Login to Purchase</Button>
									:
										<Button variant="danger" size="lg" disabled>Out of Stock</Button>
								}
								</div>
							</Card.Body>		
						</Card>
					</Col>
				</Row>
			</Container>
		)
	}
import {useState, useEffect, useContext} from "react";
import UserContext from "../UserContext";
import {Navigate, useNavigate, Link} from "react-router-dom";


import Swal from "sweetalert2";
import {Button, Form, Col} from "react-bootstrap";


export default function Registration(){

	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	//State hooks to store the values of the input fields
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");

	//Check if the values are successfully binded/passed.
	console.log(firstName);
	console.log(lastName);
	console.log(email);
	console.log(mobileNo);
	console.log(password1);
	console.log(password2);

	function registerUser(e){

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/verifyEmail`, {
			method: "POST",
			headers:{
				"Content-Type":"application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data){
				Swal.fire({
					title: "Email address already exist",
					icon: "error",
					text: "Kindly provide another email or Sign in to your account."
				})
			}
			else{
				fetch(`${process.env.REACT_APP_API_URL}/users/registration`, {
					method: "POST",
					headers:{
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data){
						setFirstName("");
						setLastName("");
						setMobileNo("");
						setEmail("");
						setPassword1("");
						setPassword2("");

						Swal.fire({
							title: "Registration successful",
							icon: "success",
							text: "You may now login."
						})

						navigate("/login");
					}
					else{
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						})
					}
				})
			}
		})

	}

	const [isActive, setIsActive] = useState(false);


	useEffect(()=>{
		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo.length === 11 && password1 !== "" && password2 !== "") && (password1 === password2)){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	},[firstName, lastName, email, mobileNo, password1, password2])

	return(
		(user.id !== null)
		?
			<Navigate to="/courses"/>
		:
		<>
			<h1 className="my-5 text-center text-white">Sign up</h1>
			<Col md={{ span: 4, offset: 4 }} >		
				<Form id="signup" className="text-light" onSubmit = {(e) => registerUser(e)}>

					<Form.Group className="m-3" controlId="firstName">
					  <Form.Label className="text-white">First Name</Form.Label>
					  <Form.Control type="text" placeholder="First Name" value={firstName} onChange={e => setFirstName(e.target.value)}/>
					</Form.Group>

					<Form.Group className="m-3" controlId="lastName">
					  <Form.Label className="text-white">Last Name</Form.Label>
					  <Form.Control type="text" placeholder="Last Name" value={lastName} onChange={e => setLastName(e.target.value)}/>
					</Form.Group>

					<Form.Group className="m-3" controlId="mobileNo">
					  <Form.Label className="text-white">Mobile Number</Form.Label>
					  <Form.Control type="number" placeholder="Mobile Number" value={mobileNo} onChange={e => setMobileNo(e.target.value)}/>
					</Form.Group>

				      <Form.Group className="m-3" controlId="userEmail">
				        <Form.Label className="text-white">Email address</Form.Label>
				        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
				        <Form.Text className="text-muted">
				          We'll never share your email with anyone else.
				        </Form.Text>
				      </Form.Group>

				      <Form.Group className="m-3" controlId="password1">
				        <Form.Label className="text-white">Password</Form.Label>
				        <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)}/>
				      </Form.Group>

				      <Form.Group className="m-3" controlId="password2">
				        <Form.Label className="text-white">Verify Password</Form.Label>
				        <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)}/>
				      </Form.Group>
			      
			      {
			      	isActive
			      	?
			      		<Button className="ms-3" variant="primary" type="submit" id="submitBtn">
			      		  Submit
			      		</Button>
			      	:
			      		<Button className="ms-3" variant="primary" type="submit" id="submitBtn" disabled>
			      		  Submit
			      		</Button>
			      }

			      	<p className="m-3 text-white"> Already have an account? <Link to="/login" className="text-decoration-none">Login </Link> here</p>
			      	
			    </Form>
		    </Col>
		</>
	)
}
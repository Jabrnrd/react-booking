//Applying bootstrap grid system
import {Row, Col, Card, Carousel} from "react-bootstrap";

export default function Highlights(){
	return(
		<Row className="mt-3 mb-3">
		    <Col md={{ span: 8, offset: 2 }}>
		        <Card id="Highlights"className="cardHighlight p-3">
		            <Card.Body>
		            <Card.Title>
		                    <h2 className="text-center text-white">Best Seller Games</h2>
		                </Card.Title>
		                
		                <Carousel id="carouselHighlights" variant="dark">
		                      <Carousel.Item interval={2000}>
		                        <img
		                          className="d-block w-100"
		                          src="https://wallpaperaccess.com/full/7948004.jpg"
		                          alt="First slide"
		                        />
		                        
		                      </Carousel.Item>
		                      <Carousel.Item interval={2000}>
		                        <img
		                          className="d-block w-100"
		                          src="https://wallpaperaccess.com/full/3511118.jpg"
		                          alt="Second slide"
		                        />
		                        
		                      </Carousel.Item>
		                      <Carousel.Item interval={2000}>
		                        <img
		                          className="d-block w-100"
		                          src="https://wallpaperaccess.com/full/38280.jpg"
		                          alt="Third slide"
		                        />
		                      </Carousel.Item>
		                    </Carousel>
		            </Card.Body>
		        </Card>
		    </Col>
		</Row>
	)
}
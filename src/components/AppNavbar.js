import { Link } from "react-router-dom";
import {useContext} from "react";
import UserContext from "../UserContext"

import {Navbar, Nav, Container} from "react-bootstrap";


export default function AppNavbar(){

	const {user} = useContext(UserContext);

	return(
		<Navbar id="navbar" bg="dark" expand="lg">
	      <Container>
	        <Navbar.Brand as={Link} to="/" className="text-dark"><img src="https://drive.google.com/uc?export=view&id=1chw4LFP6GMWFolCaTkmA0LarXo7MecSS" alt="..." width="150px" height="50px" /></Navbar.Brand>
	        <Navbar.Toggle aria-controls="basic-navbar-nav" />
	        <Navbar.Collapse id="basic-navbar-nav">
	          <Nav className="ms-auto" defaultActiveKey="/">
	            <Nav.Link as={Link} to="/" eventKey="/"><img src="https://drive.google.com/uc?export=view&id=1vYhyRnWngxkOLffjjSNW3m8DLKiwLpRc" alt="..." width="125px" height="25px" /></Nav.Link>
	            {
	            	(user.isAdmin)
	            	?
	            		<Nav.Link as={Link} to="/admin" eventKey="/admin"><img src="https://drive.google.com/uc?export=view&id=1xbb3fvK0ab92U0TSobjQAQB2NIyNf7s2" alt="..." width="120px" height="25px" /></Nav.Link> 
	            	:
	            		<Nav.Link as={Link} to="/products" eventKey="/products"><img src="https://drive.google.com/uc?export=view&id=1VcZGv_HpCO8ChuotHGEMOWQ4YojfLhRQ" alt="..." width="120px" height="25px" /></Nav.Link>

	            }
	            {
	            	(user.id !== null)
	            	?
	            		<Nav.Link as={Link} to="/logout" eventKey="/logout"><img src="https://drive.google.com/uc?export=view&id=1Kg20og37ZadE8H2Z18qzUy8w9B79o782" alt="..." width="120px" height="25px" /></Nav.Link>
	            	:
	            		<>
		            		<Nav.Link as={Link} to="/registration" eventKey="/registration"><img src="https://drive.google.com/uc?export=view&id=18IcQSwmOYcJiGWJVlFp7dQTtaQRpRTRr" alt="..." width="120px" height="25px" /></Nav.Link>
		            		<Nav.Link as={Link} to="/login" eventKey="/login"><img src="https://drive.google.com/uc?export=view&id=1nhtjIj6CPHR-XwXVOYnQMUxv_WmwBMUy" alt="..." width="120px" height="25px" /></Nav.Link>
	            		</>
	            }
	          </Nav>
	        </Navbar.Collapse>
	      </Container>
	    </Navbar>
	)
}
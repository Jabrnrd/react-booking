import {Row, Col, Button} from "react-bootstrap";
import {Link} from "react-router-dom";

export default function Banner({data}){
	console.log(data);

	const {title, content, destination, label} = data;

	return(
		<Row>
			<Col className="p-5 text-center">
				<h1 className="mb-3 text-light">{title}</h1>
				<h4 className="mb-5 text-light ">{content}</h4>
				<Button as={Link} to={destination} variant="btn btn-outline-secondary" size="lg">{label}</Button>
			</Col>
		</Row>
	)
}
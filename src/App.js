import {useState, useEffect} from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { UserProvider } from "./UserContext";

import AppNavbar from "./components/AppNavbar";

// import Home from "./pages/Home";
import AdminDashboard from "./pages/AdminDashboard"
import AddProduct from "./pages/AddProduct"; 
import EditProduct from "./pages/EditProduct"
import Home from "./pages/Home"
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import NoLink from "./pages/NoLink"
import Products from "./pages/Products"
import Registration from "./pages/Registration";
import UserDashboard from "./pages/UserDashboard"
import ViewProduct from "./pages/ViewProduct";


import {Container} from "react-bootstrap";


import './App.css';


function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () =>{
    localStorage.clear();
  }


  useEffect(() =>{
    console.log(user);
    console.log(localStorage);
  }, [user])

  useEffect(() =>{
    fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
      headers:{
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if(typeof data._id !== "undefined"){
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
      }
      else{
          setUser({
              id: null,
              isAdmin: null
            })
      } 
    })
  }, [])

  return (
      <UserProvider value={{user, setUser, unsetUser}}>
          <Router>
            <AppNavbar />
            <Container fluid>
                <Routes>
                    <Route exact path ="/" element={<Home />} />
                    <Route exact path ="/admin" element={<AdminDashboard />} />
                    <Route exact path ="/addProduct" element={<AddProduct />} />
                    <Route exact path="/editProduct/:productId" element={<EditProduct />}/>
                    <Route exact path="/products/:productId" element={<ViewProduct />}/>
                    <Route exact path ="/products" element={<Products />} />
                    <Route exact path ="/userDetails" element={<UserDashboard />} />
                    <Route exact path ="/registration" element={<Registration />} />
                    <Route exact path ="/login" element={<Login />} />
                    <Route exact path ="/logout" element={<Logout />} />
                    <Route exact path ="*" element={<NoLink />} />
                </Routes>
            </Container>
          </Router>
      </UserProvider>
  );
}

export default App;
